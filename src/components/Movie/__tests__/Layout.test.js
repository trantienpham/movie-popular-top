import React from "react";
import { shallow } from "enzyme";
import Layout from "../Layout";

describe("Layout", () => {
  it("should render properly", done => {
    const wrapper = shallow(<Layout />);
    expect(wrapper.getElements()).toMatchSnapshot();
    done();
  });
});
