import React from "react";
import { mount } from "enzyme";
import { SearchBar } from "../SearchBar";

describe("SearchBar", () => {
  it("should render properly", done => {
    const wrapper = mount(<SearchBar onSearch={() => {}} />);
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    done();
  });
  it("should render properly", done => {
    const onSearch = jest.fn();
    const submittor = {
      target: [{ value: "new todo 1" }],
      preventDefault: jest.fn()
    };
    const wrapper = mount(<SearchBar onSearch={onSearch} />);
    wrapper.find("form#search-form").simulate("submit", submittor);
    expect(onSearch).toHaveBeenCalledTimes(1);
    expect(submittor.preventDefault).toHaveBeenCalledTimes(1);
    done();
  });
});
