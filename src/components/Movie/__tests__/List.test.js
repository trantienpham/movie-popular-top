import React from "react";
import { mount } from "enzyme";
import List from "../List";

describe("List", () => {
  const movies = [
    {
      popularity: 31.427,
      vote_count: 13362,
      video: false,
      poster_path: "/9zDwvsISU8bR15R2yN3kh1lfqve.jpg",
      id: 10195,
      adult: false,
      backdrop_path: "/LvmmDZxkTDqp0DX7mUo621ahdX.jpg",
      original_language: "en",
      original_title: "Thor",
      genre_ids: [28, 12, 14],
      title: "Thor",
      vote_average: 6.7,
      overview:
        "Against his father Odin's will, The Mighty Thor - a powerful but arrogant warrior god - recklessly reignites an ancient war. Thor is cast down to Earth and forced to live among humans as punishment. Once here, Thor learns what it takes to be a true hero when the most dangerous villain of his world sends the darkest forces of Asgard to invade Earth.",
      release_date: "2011-05-06"
    }
  ];

  it("should render Loading", done => {
    const wrapper = mount(<List movies={[]} />);
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    expect(html).toContain("Loading...");
    done();
  });
  it("should show error", done => {
    const wrapper = mount(<List isLoading={false} movies={[]} error={{}} />);
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    expect(html).toContain("There is errors during fetching data.");
    done();
  });
  it("should show empty items", done => {
    const wrapper = mount(<List isLoading={false} movies={[]} error={null} />);
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    expect(html).toContain(
      "There is not movie which matchs with your expectation."
    );
    done();
  });
  it("should has an item", done => {
    const wrapper = mount(
      <List isLoading={false} movies={movies} error={null} />
    );
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    expect(html).toContain(movies[0].overview);
    done();
  });
});
