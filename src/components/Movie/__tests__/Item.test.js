import React from "react";
import { mount } from "enzyme";
import Item from "../Item";

describe("Item", () => {
  const movie = {
    popularity: 31.427,
    vote_count: 13362,
    video: false,
    poster_path: "/9zDwvsISU8bR15R2yN3kh1lfqve.jpg",
    id: 10195,
    adult: false,
    backdrop_path: "/LvmmDZxkTDqp0DX7mUo621ahdX.jpg",
    original_language: "en",
    original_title: "Thor",
    genre_ids: [28, 12, 14],
    title: "Thor",
    vote_average: 6.7,
    overview:
      "Against his father Odin's will, The Mighty Thor - a powerful but arrogant warrior god - recklessly reignites an ancient war. Thor is cast down to Earth and forced to live among humans as punishment. Once here, Thor learns what it takes to be a true hero when the most dangerous villain of his world sends the darkest forces of Asgard to invade Earth.",
    release_date: "2011-05-06"
  };
  const wrapper = mount(<Item movie={movie} />);

  it("should render properly", done => {
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    done();
  });
});
