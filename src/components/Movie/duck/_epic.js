import { ofType, combineEpics } from "redux-observable";
import { FETCH_POPULAR_MOVIES, FETCH_TOP_MOVIES } from "./_types";
import { withLatestFrom, switchMap, map, catchError } from "rxjs/operators";
import * as stateManager from "./_stateManager";
import * as fetchers from "../../../fetchers/movie";
import * as actions from "./_actions";
import { from, of, merge } from "rxjs";

export function fetch(fetcher, successAction, errorAction) {
  return from(fetcher()).pipe(
    map(response => successAction(response)),
    catchError(error => of(errorAction(error)))
  );
}

export const fetchPopularMoviesEpic = (action$, state$) => {
  return action$.pipe(
    ofType(FETCH_POPULAR_MOVIES),
    withLatestFrom(state$),
    switchMap(([, state]) =>
      merge(
        of(actions.loadPopularMoviesProcessing()),
        fetch(
          () => {
            const params = stateManager.getSearchString(state);
            return fetchers.getPopularMovies(params);
          },
          actions.loadPopularMoviesSuccess,
          actions.loadPopularMoviesError
        )
      )
    )
  );
};

export const fetchTopMoviesEpic = (action$, state$) => {
  return action$.pipe(
    ofType(FETCH_TOP_MOVIES),
    withLatestFrom(state$),
    switchMap(([, state]) =>
      merge(
        of(actions.loadTopMoviesProcessing()),
        fetch(
          () => {
            const params = stateManager.getSearchString(state);
            return fetchers.getTopMovies(params);
          },
          actions.loadTopMoviesSuccess,
          actions.loadTopMoviesError
        )
      )
    )
  );
};

export default combineEpics(fetchPopularMoviesEpic, fetchTopMoviesEpic);
