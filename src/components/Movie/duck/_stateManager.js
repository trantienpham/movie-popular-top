import _get from "lodash/fp/get";
import _merge from "lodash/fp/merge";
import _isNil from "lodash/fp/isNil";
import _omit from "lodash/fp/omit";

/********************************************
 * STATE STRUCTURE
 ********************************************/
export const INIT_STATE = {
  searchString: "",
  popular: {
    error: undefined,
    movies: []
  },
  topRate: {
    error: undefined,
    movies: []
  }
};

/********************************************
 * STATE SETTERS
 ********************************************/
export function setPopular(state, payload) {
  const newState = _merge(
    state,
    _isNil(payload) ? {} : { popular: _omit("movies", payload) }
  );
  if (!_isNil(payload)) {
    newState.popular.movies = payload.movies;
  }
  newState.popular.movies = newState.popular.movies || [];
  return newState;
}
export function setTopRate(state, payload) {
  const newState = _merge(
    state,
    _isNil(payload) ? {} : { topRate: _omit("movies", payload) }
  );
  if (!_isNil(payload)) {
    newState.topRate.movies = payload.movies;
  }
  newState.topRate.movies = newState.topRate.movies || [];
  return newState;
}
export function setSearchString(state, payload) {
  return _merge(state, { searchString: payload });
}
/********************************************
 * STATE GETTERS
 ********************************************/
export function getPopular(state) {
  return _get("popular", state);
}
export function getTopRate(state) {
  return _get("topRate", state);
}
export function getSearchString(state) {
  return _get("searchString", state);
}
