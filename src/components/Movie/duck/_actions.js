import * as types from "./_types";
import merge from "lodash/fp/merge";

/********************************************
 * POPULAR MOVIES
 ********************************************/
export function loadPopularMovies() {
  return { type: types.FETCH_POPULAR_MOVIES, payload: null };
}
export function loadPopularMoviesProcessing(payload) {
  return {
    type: types.FETCH_POPULAR_MOVIES_PROCESSING,
    payload: merge(payload || {}, { isLoading: true })
  };
}
export function loadPopularMoviesSuccess(data) {
  return {
    type: types.FETCH_POPULAR_MOVIES_SUCCESS,
    payload: { movies: data, isLoading: false }
  };
}
export function loadPopularMoviesError(error) {
  return {
    type: types.FETCH_POPULAR_MOVIES_ERROR,
    payload: { error, isLoading: false }
  };
}

/********************************************
 * TOP MOVIES
 ********************************************/
export function loadTopMovies() {
  return { type: types.FETCH_TOP_MOVIES, payload: null };
}
export function loadTopMoviesProcessing(payload) {
  return {
    type: types.FETCH_TOP_MOVIES_PROCESSING,
    payload: merge(payload || {}, { isLoading: true })
  };
}
export function loadTopMoviesSuccess(data) {
  return {
    type: types.FETCH_TOP_MOVIES_SUCCESS,
    payload: { movies: data, isLoading: false }
  };
}
export function loadTopMoviesError(error) {
  return {
    type: types.FETCH_TOP_MOVIES_ERROR,
    payload: { error, isLoading: false }
  };
}

/********************************************
 * SEARCH MOVIES
 ********************************************/
export function searchMoviesByTerm(searchString) {
  return { type: types.SEARCH_MOVIES, payload: searchString };
}
