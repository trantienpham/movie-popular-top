/* eslint-disable import/first */
import { ActionsObservable, ofType } from "redux-observable";
import { fetchPopularMoviesEpic, fetchTopMoviesEpic } from "../_epic";
import * as actions from "../_actions";
import * as types from "../_types";
import { of } from "rxjs";
jest.mock("../../../../fetchers/movie", () => ({
  getPopularMovies: jest.fn(),
  getTopMovies: jest.fn()
}));
import { getPopularMovies, getTopMovies } from "../../../../fetchers/movie";

describe("_epic", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("fetchPopularMoviesEpic", () => {
    it("should be successful", done => {
      getPopularMovies.mockImplementation(() => Promise.resolve([]));
      const action$ = ActionsObservable.of(actions.loadPopularMovies());
      const state$ = of({});
      const output = fetchPopularMoviesEpic(action$, state$);
      output
        .pipe(ofType(types.FETCH_POPULAR_MOVIES_SUCCESS))
        .subscribe(action => {
          expect(action.type).toBe(types.FETCH_POPULAR_MOVIES_SUCCESS);
          expect(action.payload).toEqual({ movies: [], isLoading: false });
          done();
        });
    });
    it("should throw error", done => {
      getPopularMovies.mockImplementation(() => Promise.reject("error"));
      const action$ = ActionsObservable.of(actions.loadPopularMovies());
      const state$ = of({});
      const output = fetchPopularMoviesEpic(action$, state$);
      output
        .pipe(ofType(types.FETCH_POPULAR_MOVIES_ERROR))
        .subscribe(action => {
          expect(action.type).toBe(types.FETCH_POPULAR_MOVIES_ERROR);
          expect(action.payload).toEqual({ error: "error", isLoading: false });
          done();
        });
    });
  });

  describe("fetchTopMoviesEpic", () => {
    it("should be successful", done => {
      getTopMovies.mockImplementation(() =>
        Promise.resolve([{ name: "name" }])
      );
      const action$ = ActionsObservable.of(actions.loadTopMovies());
      const state$ = of({});
      const output = fetchTopMoviesEpic(action$, state$);
      output.pipe(ofType(types.FETCH_TOP_MOVIES_SUCCESS)).subscribe(action => {
        expect(action.type).toBe(types.FETCH_TOP_MOVIES_SUCCESS);
        expect(action.payload).toEqual({
          movies: [{ name: "name" }],
          isLoading: false
        });
        done();
      });
    });
    it("should throw error", done => {
      getTopMovies.mockImplementation(() => Promise.reject("error"));
      const action$ = ActionsObservable.of(actions.loadTopMovies());
      const state$ = of({});
      const output = fetchTopMoviesEpic(action$, state$);
      output.pipe(ofType(types.FETCH_TOP_MOVIES_ERROR)).subscribe(action => {
        expect(action.type).toBe(types.FETCH_TOP_MOVIES_ERROR);
        expect(action.payload).toEqual({ error: "error", isLoading: false });
        done();
      });
    });
  });
});
