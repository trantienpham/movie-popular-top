import * as types from "./_types";
import {
  INIT_STATE,
  setSearchString,
  setPopular,
  setTopRate
} from "./_stateManager";

export default (state = INIT_STATE, { type, payload } = {}) => {
  switch (type) {
    case types.FETCH_POPULAR_MOVIES:
    case types.FETCH_POPULAR_MOVIES_PROCESSING:
    case types.FETCH_POPULAR_MOVIES_SUCCESS:
    case types.FETCH_POPULAR_MOVIES_ERROR:
      return setPopular(state, payload);
    case types.FETCH_TOP_MOVIES:
    case types.FETCH_TOP_MOVIES_PROCESSING:
    case types.FETCH_TOP_MOVIES_SUCCESS:
    case types.FETCH_TOP_MOVIES_ERROR:
      return setTopRate(state, payload);

    case types.SEARCH_MOVIES:
      return setSearchString(state, payload);
    default:
      return state;
  }
};
