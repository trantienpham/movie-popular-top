import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default ({ movie }) => {
  const {
    title,
    overview,
    release_date,
    original_language,
    adult,
    poster_path
  } = movie;
  return (
    <Row className="movie-item">
      <Col md="1">
        <img
          src={`https://image.tmdb.org/t/p/w500${poster_path}`}
          style={{ width: "100px", height: "100px" }}
          alt={title}
        />
      </Col>
      <Col md="11">
        <div className="alg-left mrg-left title">{title}</div>
        <div className="alg-left mrg-left overview">{overview}</div>
        <div className="alg-left mrg-left release-date">
          <strong>Release Date:</strong> {release_date}
        </div>
        <div className="alg-left mrg-left language">
          <strong>Language:</strong> {original_language}
        </div>
        <div className="alg-left mrg-left age">
          <strong>Age: </strong>
          {adult ? "18+" : "all ages"}
        </div>
      </Col>
    </Row>
  );
};
