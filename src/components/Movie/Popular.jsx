import React, { useEffect } from "react";
import { connect } from "react-redux";
import { stateManager, actions } from "./duck";
import isNil from "lodash/fp/isNil";
import List from "./List";

export default connect(
  state => stateManager.getPopular(state),
  dispatch => ({
    loadMovies() {
      dispatch(actions.loadPopularMovies());
    }
  })
)(({ isLoading, loadMovies, ...others }) => {
  useEffect(() => {
    if (isNil(isLoading)) {
      loadMovies();
    }
  });

  return <List isLoading={isLoading} {...others} />;
});
