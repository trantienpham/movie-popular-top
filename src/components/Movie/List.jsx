import React, { Fragment, useState, useCallback } from "react";
import Row from "react-bootstrap/Row";
import isNil from "lodash/fp/isNil";
import slice from "lodash/fp/slice";
import Item from "./Item";
import { Loading } from "../Loading";
import Pagination from "../pagination";

const pageSize = 5;
export default ({ isLoading, title, movies, error }) => {
  const [pageIndex, setPageIndex] = useState(1);
  const numPages = Math.ceil(movies.length / pageSize);
  const skip = (pageIndex - 1) * pageSize;
  const take = skip + pageSize;
  const pagedMovies = slice(skip, take, movies);
  const onPageChange = useCallback(
    page => {
      if (page === pageIndex) return;
      setPageIndex(page);
    },
    [pageIndex]
  );

  return (
    <Fragment>
      <Row>
        <h3>{title}</h3>
      </Row>
      <Row className="movie-list">
        {(isNil(isLoading) || isLoading) && <Loading />}
        {!isNil(error) && <div>There is errors during fetching data.</div>}
        {numPages === 0 && (
          <div>There is not movie which matchs with your expectation.</div>
        )}
        {isNil(error) &&
          pagedMovies.map(movie => <Item key={movie.id} movie={movie} />)}
      </Row>
      {numPages > 1 && (
        <Pagination
          page={pageIndex}
          numPages={numPages}
          onPageChange={onPageChange}
        />
      )}
    </Fragment>
  );
};
