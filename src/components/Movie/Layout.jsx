import React from "react";
import Popular from "./Popular";
import TopRate from "./TopRate";
import SearchBar from "./SearchBar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import "./styles.scss";

export default () => {
  return (
    <Container className="movie-module">
      <Row className="search-bar">
        <SearchBar />
      </Row>
      <TopRate title="Top movies" />
      <Popular title="Most Popular" />
    </Container>
  );
};
