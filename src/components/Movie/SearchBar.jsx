import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import { connect } from "react-redux";
import { actions } from "./duck";

export const SearchBar = ({ onSearch }) => {
  const [value, setValue] = useState("");
  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        onSearch(value);
      }}
      id="search-form"
    >
      <Form.Control
        value={value}
        onChange={({ target }) => setValue(target.value)}
        placeholder="Search movie title"
        id="search-term"
      />
    </Form>
  );
};

export default connect(
  null,
  dispatch => ({
    onSearch(searchTerm) {
      dispatch(actions.searchMoviesByTerm(searchTerm));
      dispatch(actions.loadPopularMovies());
      dispatch(actions.loadTopMovies());
    }
  })
)(SearchBar);
