import React from "react";
import Pagination from "react-bootstrap/Pagination";
import Row from "react-bootstrap/Row";

function createPages(begin, end) {
  const pages = [];
  for (let page = begin; page <= end; page++) {
    pages.push(page);
  }
  return pages;
}
function renderPageRange(page, numPages, onPageChange) {
  const totalPaginate = 5;
  const pageRange = 2;
  let shift = 0;
  let pages = [];
  // render all if less than 5 pages
  if (numPages <= totalPaginate) {
    pages = createPages(1, numPages);
  } else {
    pages = createPages(page - pageRange, page + pageRange);
    if (pages[0] <= 0) shift = 1 - pages[0];
    else if (pages[totalPaginate - 1] > numPages) {
      shift = numPages - pages[totalPaginate - 1];
    }

    pages = pages.map(page => page + shift);
  }
  return pages.map(pageNumber => (
    <Pagination.Item
      key={pageNumber}
      active={pageNumber === page}
      onClick={() => onPageChange(pageNumber)}
    >
      {pageNumber}
    </Pagination.Item>
  ));
}
export default ({ page, numPages, onPageChange }) => {
  return (
    <Row>
      <Pagination>
        <Pagination.First
          disabled={page === 1}
          onClick={() => onPageChange(1)}
        />
        <Pagination.Prev
          disabled={page < 2}
          onClick={() => onPageChange(page - 1)}
        />
        {renderPageRange(page, numPages, onPageChange)}
        <Pagination.Next
          disabled={page > numPages - 2}
          onClick={() => onPageChange(page + 1)}
        />
        <Pagination.Last
          disabled={page === numPages}
          onClick={() => onPageChange(numPages)}
        />
      </Pagination>
    </Row>
  );
};
