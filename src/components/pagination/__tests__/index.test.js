import React from "react";
import { mount } from "enzyme";
import Pagination from "../index";

describe("Pagination", () => {
  const wrapper = mount(<Pagination page={1} numPages={1} />);

  it("should render properly", done => {
    const html = wrapper.html();
    expect(html).toMatchSnapshot();
    expect(wrapper.find(".page-item")).toHaveLength(5);
    done();
  });
});
