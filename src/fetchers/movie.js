import { from } from "rxjs";
import { concatAll, toArray } from "rxjs/operators";
import isEmpty from "lodash/fp/isEmpty";

const apiKey = "7a77ac262dee6a380a38e9e391317b9f";
const popularMovies = [];
const topMovies = [];

function searchMovies(searchString, movies) {
  return isEmpty(searchString)
    ? movies
    : movies.filter(movie =>
        (movie.title || "")
          .toLowerCase()
          .trim()
          .includes(searchString.toLowerCase())
      );
}

export function getPopularMovies(searchString = "") {
  return new Promise((resolve, reject) => {
    if (popularMovies.length > 0) {
      return resolve(popularMovies);
    }
    from([
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=1`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=2`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=3`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=4`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=5`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=6`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=7`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=8`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=9`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}&language=en-US&page=10`
      )
    ])
      .pipe(
        concatAll(),
        toArray()
      )
      .subscribe(
        responses => {
          from(responses.map(response => response.json()))
            .pipe(
              concatAll(),
              toArray()
            )
            .subscribe(
              movies => {
                movies.forEach(movie => {
                  popularMovies.push(...movie.results);
                });
                resolve(popularMovies);
              },
              err => reject(err)
            );
        },
        error => reject(error)
      );
  }).then(movies => searchMovies(searchString, movies));
}

export function getTopMovies(searchString = "") {
  return new Promise((resolve, reject) => {
    if (topMovies.length > 0) {
      return resolve(topMovies);
    }
    from([
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=1`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=2`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=3`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=4`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=5`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=6`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=7`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=8`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=9`
      ),
      fetch(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=10`
      )
    ])
      .pipe(
        concatAll(),
        toArray()
      )
      .subscribe(
        responses => {
          from(responses.map(response => response.json()))
            .pipe(
              concatAll(),
              toArray()
            )
            .subscribe(
              movies => {
                movies.forEach(movie => {
                  topMovies.push(...movie.results);
                });
                resolve(topMovies);
              },
              err => reject(err)
            );
        },
        error => reject(error)
      );
  }).then(movies => searchMovies(searchString, movies));
}
