import React from "react";
import { Provider } from "react-redux";
import { createStore } from "./store";
import Movie from "./components/Movie";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";

const store = createStore();
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Movie />
      </Provider>
    </div>
  );
}

export default App;
