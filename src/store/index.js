import { createEpicMiddleware } from "redux-observable";
import { createLogger } from "redux-logger";
import { createStore as createReduxStore, applyMiddleware } from "redux";
import { epic, reducer } from "../components/Movie/duck";

export function createStore() {
  const epicMiddleware = createEpicMiddleware();
  const loggerMiddleware = createLogger();
  const enhancers = applyMiddleware(loggerMiddleware, epicMiddleware);
  const store = createReduxStore(reducer, enhancers);

  epicMiddleware.run(epic);
  return store;
}
